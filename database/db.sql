CREATE DATABASE IF NOT EXISTS db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

use db;

CREATE TABLE IF NOT EXISTS clients (
  id INT(6) UNSIGNED AUTO_INCREMENT,
  nom TEXT NOT NULL,
  prenom TEXT NOT NULL,
  email VARCHAR(120) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE(email)
);

CREATE TABLE IF NOT EXISTS voitures (
  id INT(6) UNSIGNED AUTO_INCREMENT,
  marque VARCHAR(40) NOT NULL,
  type_moteur VARCHAR(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS client_voiture (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  client_id INT(6) UNSIGNED NOT NULL,
  voiture_id INT(6) UNSIGNED NOT NULL
);

INSERT INTO clients (nom, prenom, email)
VALUES 
  ("Doe", "John", "john@email.co"),
  ("Mary", "Jane", "jane@email.co"),
  ("Pence", "Mike", "mike@email.co");

INSERT INTO voitures (marque, type_moteur)
VALUES 
  ("Renault", "Diesel"),
  ("Toyota", "Essence"),
  ("Nissan", "Hybride");

-- John a la Renault
INSERT INTO client_voiture (client_id, voiture_id) VALUES (1, 1);

-- Mary a la Toyota et la Nissan
INSERT INTO client_voiture (client_id, voiture_id) VALUES (2, 2);
INSERT INTO client_voiture (client_id, voiture_id) VALUES (2, 3);

-- Pence a la Nissan (la Nissan appartiennent a Mary et Pence)
INSERT INTO client_voiture (client_id, voiture_id) VALUES (3, 3);