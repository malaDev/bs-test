<?php

require_once dirname(dirname(__FILE__)) . "/helpers.inc.php";

define("DATABASE_DSN", array_get($_ENV, "DATABASE_DSN", "mysql:dbname=db;host=localhost"));
define("DATABASE_USER", array_get($_ENV, "DATABASE_USER", "root"));
define("DATABASE_PASSWORD", array_get($_ENV, "DATABASE_PASSWORD", "root"));

$pdo = new PDO(DATABASE_DSN, DATABASE_USER, DATABASE_PASSWORD);

return $pdo;