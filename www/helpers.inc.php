<?php

function array_get($arr, $key, $default = null) {
    if (array_key_exists($key, $arr)) {
        return $arr[$key];
    }

    return $default;
}

// clients
function clients_fetch() {
    global $pdo;

    $s = $pdo->query("select * from clients");
    $s->execute();

    return $s->fetchAll(PDO::FETCH_ASSOC);
}

function clients_get($clientId) {
    global $pdo;

    $s = $pdo->prepare("select * from clients where id = ?");
    $s->execute([$clientId]);

    return $s->fetch(PDO::FETCH_ASSOC);
}

function clients_update($clientId, $data) {
    global $pdo;

    if (empty($data)) {
        return false;
    }

    $cols = array_map(function ($key) {
        return sprintf("%s=:%s", $key, $key);
    }, array_keys($data));

    $s = $pdo->prepare(
        str_replace("__COLS__", implode(", ", $cols), "update clients set __COLS__ where id=:id")
    );

    $data["id"] = $clientId;
    return $s->execute($data);
}

// voitures
function voitures_fetch() {
    global $pdo;

    $s = $pdo->prepare("
      select v.*, c.nom nom_client, c.prenom prenom_client, c.email from voitures v 
      right join client_voiture as map on map.voiture_id = v.id 
      left join clients c on map.client_id = c.id
  ");
    $s->execute();

    $result = $s->fetchAll(PDO::FETCH_ASSOC);

    // group by Id
    $_results = [];
    foreach ($result as $line) {
        if (array_key_exists($line["id"], $_results)) {
            $_results[$line["id"]]["proprietaires"][] = trim(sprintf("%s %s", $line["nom_client"],
                $line["prenom_client"]));
            continue;
        }

        $_results[$line["id"]] = [
            "id"            => $line["id"],
            "marque"        => $line["marque"],
            "type_moteur"   => $line["type_moteur"],
            "proprietaires" => [
                trim(sprintf("%s %s", $line["nom_client"], $line["prenom_client"])),
            ],
        ];
    }

    return array_values($_results);
}

;