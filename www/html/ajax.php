<?php

require_once dirname(dirname(__FILE__)) . "/config/database.inc.php";

header("Content-Type: application/json");

function ajax($req) {
    $call = explode(".", $req["__call"]);
    $args = $req["__args"];

    $fnMap = array_get([
        "clients" => [
            "get"    => "clients_get",
            "update" => "clients_update",
        ],
    ], $call[0], false);

    if (!$fnMap || !array_key_exists($call[1], $fnMap)) {
        http_response_code(405);

        return [
            "error"   => true,
            "message" => sprintf("'%s' call not supported", $req["__call"]),
        ];
    }

    $fnCall = $fnMap[$call[1]];

    try {
        $response = call_user_func_array($fnCall, $args);
    } catch(\Exception $e) {
        http_response_code(500);
        $response = [
            "error"   => true,
            "message" => $e->getMessage(),
        ];
    }

    return $response;
}

$bodyResponse = ajax(array_merge($_REQUEST, $_GET, $_POST));

echo json_encode(
    is_array($bodyResponse) ? $bodyResponse : [$bodyResponse]
);