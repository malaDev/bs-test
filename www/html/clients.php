<?php require_once dirname(dirname(__FILE__)) . "/config/database.inc.php"; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Test - Clients</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="/assets/css/form-validation.css" rel="stylesheet">
</head>
<body class="bg-light">

<div class="container">
    <main>
        <div class="py-5 text-center">
            <a href="/"><img class="d-block mx-auto mb-4"
                             src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="72"
                             height="57"></a>
            <h2>Clients</h2>
        </div>

        <div class="row g-5">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Email</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (clients_fetch() as $line) { ?>

                    <tr>
                        <th scope="row"><?php echo $line["id"]; ?></th>
                        <td><?php echo $line["email"]; ?></td>
                        <td><?php echo $line["nom"]; ?></td>
                        <td><?php echo $line["prenom"]; ?></td>
                        <td>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#updateModal"
                               data-bs-id="<?php echo $line["id"]; ?>">Modifier</a>
                        </td>
                    </tr>

                <?php } ?>
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true"
             data-bs-backdrop="static" data-bs-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateModalLabel">Modifier</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div id="error-message" class="alert alert-danger d-none" role="alert"></div>
                        <div class="mb-3">
                            <label for="lastNameInput" class="form-label">Nom</label>
                            <input name="nom" id="lastNameInput" class="form-control" type="text"
                                   placeholder="Nom du client">
                        </div>
                        <div class="mb-3">
                            <label for="firstNameInput" class="form-label">Prenom</label>
                            <input name="prenom" id="firstNameInput" class="form-control" type="text"
                                   placeholder="Prenom du client">
                        </div>
                        <div class="mb-3">
                            <label for="emailInput" class="form-label">Email</label>
                            <input name="email" id="emailInput" class="form-control" type="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button id="trigger-update" type="button" class="btn btn-primary">Enregistrer</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    (function ($) {
        const updateModal = document.getElementById('updateModal');
        const $errBox = $("#error-message");

        async function launchRequest(call, args) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: "/ajax.php",
                    method: "POST",
                    data: {
                        __call: call,
                        __args: args,
                    },
                    success: resolve
                }).fail((_, status, err) => {
                    reject(err);
                });
            })
        }

        $(updateModal).on('show.bs.modal', function (event) {
            const button = event.relatedTarget
            const clientId = button.getAttribute('data-bs-id')
            const modalTitle = updateModal.querySelector('.modal-title')

            modalTitle.textContent = 'Modifier Client #' + clientId;

            launchRequest("clients.get", [clientId]).then(
                (response, status) => {
                    ['nom', 'prenom', 'email'].forEach(prop => {
                        $(`[name=${prop}]`).val(response[prop])
                    })
                }
            ).catch(() => {
                $errBox
                    .removeClass('d-none')
                    .text("Un erreur est survenue, veuillez recommencer");
                setTimeout(() => {
                    $errBox.addClass('d-none')
                    $(updateModal).modal("hide")
                }, 2000)
            })

            $("#trigger-update").click(() => {
                const data = {};
                ['nom', 'prenom', 'email'].forEach(prop => {
                    data[prop] = $(`[name=${prop}]`).val();
                });

                launchRequest("clients.update", [clientId, data]).then(
                    () => {
                        location.reload();
                    }
                ).catch((e) => {
                    $errBox
                        .text(e.message)
                        .removeClass('d-none');
                })
            })
        });
    })(jQuery)
</script>
</body>
</html>
