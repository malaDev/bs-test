# Simple sandbox for PHP native development #

## Setup & Configuration ##
If docker is installed with docker-compose, just run `docker-compose up -d`
everything will be available on http://localhost:18080

## Où est quoi? ##
- Structure et contenu de la base de données: `database/db.sql`
- Logique server: `www`, il faudrait s'assurer que `www/html` soit le `DOCUMENT_ROOT`